all:
	pdflatex CompStatCheatSheat.tex
	pdflatex CompStatCheatSheat.tex

clean:
	rm -rf *.aux
	rm -rf *.log
	rm -rf *.out
	rm -rf *.pdf
